const path = require('path');
const { merge } = require('webpack-merge');
const commonWebPackConfiguration = require('./webpack.common');

module.exports = merge(commonWebPackConfiguration, {
  entry: [path.join(__dirname, '../src/index.jsx')],
  mode: 'production',
});
