const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const BrotliWebpackPlugin = require('brotli-webpack-plugin')

module.exports = {
  output: {
    filename: '[name].js',
    path: path.join(__dirname, '../dist'),
    chunkFilename: 'chunks/[name].js',
    publicPath: '/',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        nodeModules: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
            return `npm.${packageName.replace('@', '')}`
          },
        },
        sharedModules: {
          name: 'sharedModules',
          minChunks: 2,
          enforce: true,
        }
      },
    },
  },
  resolve: {
    extensions: [
      '.jsx',
      '.js',
    ],
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        use: {
          loader: 'babel-loader',
        },
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: true,
            },
          },
        ],
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebPackPlugin({
      template: './public/index.html',
      filename: './index.html',
    }),
    new BrotliWebpackPlugin({
      deleteOriginalAssets: true,
      asset: '[path].br[query]',
      minRatio: 0.8, // Only compress file if the size of the compressed file is less than 80% of the original file
      threshold: 1240, // Only files bigger than 1kb will be compressed
      test: /\.(js|css|html)$/, // File extensions to compress
    }),
  ],
}
