const express = require('express')
const expressStaticGzip = require('express-static-gzip')
const path = require('path')

const app = express()
const PORT = process.env.PORT || 3000
const root = path.join(__dirname, '../dist')

app.use(expressStaticGzip(root, {
  enableBrotli: true,
  orderPreference: ['br', 'gz'],
}))

app.listen(PORT, () => console.log('server is listening on port ' + PORT))