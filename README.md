Example how to compress at building phase with Brotli algorithm and serving the compressed web site by using an Express server

## How to run?
- Install [Volta](https://volta.sh)
- Clone the repo
- Install dependencies
- Run the project
  ```bash
  yarn start:dev
  ``````
## Brotli and Gzip Compression?
    
While they both have their origins in the LZ77 algorithm, **Gzip was designed specifically to compress files**. The library has been incorporated into a lot of different programs that need to compress files. The library was incorporated into web servers as compressing content started to be the norm. It was one of two compression algorithms specified in RFC 2616, the HTTPS 1.1 specification. While it wasn’t specifically designed for streaming operations like web servers, it was adapted to it
    
**Brotli**, on the other hand, **was specifically designed for the web**. Google recognized the need for a way to compress streams more efficiently so they designed Brotli

Both algorithms do a good job at what they were designed to do. Gzip still continues to be used on the web because it is still better than nothing at all. However, as Brotli grows in popularity, **more and more web servers are preferring Brotli over Gzip**. Given the choice of the two, Brotli is the default many servers will use

## Brotli support?
[Here](https://caniuse.com/brotli) you can check Brotli support for all browsers

## [compression](https://www.npmjs.com/package/compression) and [express-static-gzip](https://www.npmjs.com/package/express-static-gzip)
  - Compression library compress response bodies
  - Another one just serve pre-gzipped or brotli files
  - The browser knows what is the compressed type by using the [content-encoding](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Encoding) header

## Evidences
### Before
![normal](/screenshots/normal-1.png)
![normal](/screenshots/normal-2.png)
![normal](/screenshots/normal-3.png)
### After
![compressed](/screenshots/compressed-1.png)
![compressed](/screenshots/compressed-2.png)
![compressed](/screenshots/compressed-3.png)
